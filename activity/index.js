"use strict";

let userName;
let passWord;
let role;

const logIn = function() {
    userName = prompt("Enter your username:");
    passWord = prompt("Enter your password:");
    role = prompt("Enter your role (admin/teacher/rookie):");
    
    if (userName === null || userName === "" || passWord === null || passWord === "" || role === null || role === "") {
      alert("Input should not be empty.");
    } else {
      switch (role.toLowerCase()) {
        case "admin":
          alert("Welcome back to the class portal, admin!");
          break;
        case "teacher":
          alert("Thank you for logging in, teacher!");
          break;
        case "rookie":
          alert("Welcome to the class portal, student!");
          break;
        default:
          alert("Role out of range.");
      }
    }
  }
logIn()

const checkAverage = function(data1,data2,data3,data4){
    let averageData = (data1 + data2 + data3 + data4)/4
    
    if(Math.round(averageData) <= 74){
        console.log(`-Hello student your average  is: ${Math.round(averageData)}. The letter equivalent is F`)
    }else if(Math.round(averageData) >= 75 && Math.round(averageData) <= 79){
        console.log(`-Hello student your average  is: ${Math.round(averageData)}. The letter equivalent is D`)
    }else if(Math.round(averageData) >= 80 && Math.round(averageData) <= 84){
        console.log(`-Hello student your average  is: ${Math.round(averageData)}. The letter equivalent is C`)
    }else if(Math.round(averageData) >= 85 && Math.round(averageData) <= 89){
        console.log(`-Hello student your average  is: ${Math.round(averageData)}. The letter equivalent is B`)
    }else if(Math.round(averageData) >= 90 && Math.round(averageData) <= 95){
        console.log(`-Hello student your average  is: ${Math.round(averageData)}. The letter equivalent is A`)
    }else if(Math.round(averageData) < 96){
        console.log(`-Hello student your average  is: ${Math.round(averageData)}. The letter equivalent is A+`)
    }
}

// checkAverage(71,70,73,74)
// checkAverage(75,75,76,78)
// checkAverage(80,81,82,78)
// checkAverage(84,85,87,88)
// checkAverage(89,90,91,90)
// checkAverage(91,96,97,95)
